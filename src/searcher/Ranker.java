package searcher;

import java.util.HashMap;

/**
 * Created by Michael on 5/7/15.
 */
public class Ranker {

    HashMap<String, Double> ranks;
    int totalPages = 0;
    double d = 0.85;
    String[] pages;
    boolean[][] adjMatrix;
    double approx = 0.0;//get within this many percent

    public Ranker() {

    }

    public Ranker(double dampening){
        this.d = dampening;
    }

    public Ranker(double dampening, double approx){
        this.d = dampening;
        this.approx = approx;
    }

    public void initRanks(String[] newPages, boolean[][] newAdjMatrix){//adjMatrix[outgoing][incoming]
        pages = newPages;
        adjMatrix = newAdjMatrix;
        totalPages = pages.length;
        ranks = new HashMap<>();
    }

    public boolean iterate(){
        boolean keepIterating = false;
        if (pages != null && adjMatrix != null){
            if (ranks == null){
                ranks = new HashMap<>();
            }
            HashMap<String, Double> newRanks = new HashMap<>();

            double defRank = (1 - d) / totalPages; //The default rank
            Integer[] cacheOutgoing = new Integer[totalPages];
            for (int i = 0; i < totalPages; i++){
                double rank = defRank;
                double sumOfIncoming = 0;

                for (int j = 0; j < totalPages; j++){
                    if(adjMatrix[j][i]){//if a link goes from j to i
                        double jRank = defRank;
                        int jOutgoing = 0;
                        if(ranks.containsKey(pages[j])){
                            jRank = ranks.get(pages[j]);
                        }
                        if(cacheOutgoing[j] == null){
                            for (int k = 0; k < totalPages; k++){
                                if(adjMatrix[j][k]){
                                    jOutgoing++;
                                }
                            }
                            cacheOutgoing[j] = jOutgoing;
                        } else {
                            jOutgoing = cacheOutgoing[j];
                        }
                        sumOfIncoming += (jRank / jOutgoing);
                    }
                }

                rank += d * sumOfIncoming;
                if(!keepIterating && ranks.containsKey(pages[i]) && (ranks.get(pages[i]) < (1 - approx) * rank || ranks.get(pages[i]) > (1 + approx) * rank)){
                    keepIterating = true;
                } else if(!keepIterating && !ranks.containsKey(pages[i])){
                    keepIterating = true;
                }
                newRanks.put(pages[i], rank);
            }
            ranks = newRanks;
        }
        System.out.print(ranks);
        System.out.print("\n");
        return keepIterating;
    }

    public HashMap<String, Double> calcRanks(){
        while(iterate());
        return ranks;
    }

    public HashMap<String, Double> calcRanks(String[] newPages, boolean[][] newAdjMatrix){
        initRanks(newPages, newAdjMatrix);
        return calcRanks();
    }
}
