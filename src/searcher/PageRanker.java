package searcher;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Created by Michael on 5/13/2015.
 */
public class PageRanker {

	HashMap<String, ArrayList<String>> adjMatrix;//The has links the path of the page to a list of all its outgoing links
	HashMap<String, ArrayList<String>> inLinks;//So I don't have to traverse the whole graph to find all incoming links
	HashMap<String, Double> ranks;//Where the actual PageRank scores are kept

	String cachePath = null;
	boolean[][] cacheMattrix = null;

	double d = 0.85;//the dampening factor
	boolean naive = false;

	PageRanker(){
		adjMatrix = new HashMap<>();
		inLinks = new HashMap<>();
		ranks = new HashMap<>();
	}

	public HashMap<String, Double> pageRank(String path){
		try {
			if(path != cachePath){
				init(path);
				cachePath = path;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		int stepCounter = 0;
		while (step()){
			System.out.println("Iterating step " + String.format("%d", stepCounter++));
		}
		return ranks;
	}

	public HashMap<String, Double> pageRank(String path, int iterations){
		try {
			if(path != cachePath){
				init(path);
				cachePath = path;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		int stepCounter = 0;
		while (step() && stepCounter < iterations){
			System.out.println("Iterating step " + String.format("%d", stepCounter++));
		}
		return ranks;
	}

	public long pageRank(String path, double threshold){
		try {
			if(path != cachePath) {
				init(path);
				cachePath = path;
			} else {//start from scratch
				for (String page: ranks.keySet()){
					ranks.put(page, null);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		int stepCounter = 0;
		HashMap<String, Double> oldRanks = null;
		System.out.println("Iterating step " + String.format("%d", stepCounter));
		long startTime = System.currentTimeMillis();
		while (step()){
			boolean underThreshold = true;
			double maxThresh = 0;
			if(oldRanks != null && oldRanks.size() > 0){
				for (Map.Entry<String, Double> oldRank: oldRanks.entrySet()){
					double currThresh = Math.abs(oldRank.getValue() - ranks.get(oldRank.getKey())) / oldRank.getValue();

					if (maxThresh < currThresh){
						maxThresh = currThresh;
					}

					if (maxThresh < threshold){
						underThreshold = true;
					} else {
						underThreshold = false;
						break;
					}
				}
			} else {
				underThreshold = false;
			}
			if (underThreshold){
				System.out.println("Iterating step " + String.format("%d", stepCounter) + " | UNDER THRESHOLD! : " + String.format("%e", maxThresh) + " | Time: " + String.format("%d", System.currentTimeMillis() - startTime));
				break;
			} else {
				oldRanks = (HashMap<String, Double>) ranks.clone();
			}
			System.out.println("Iterating step " + String.format("%d", ++stepCounter) + " | Current threshold: " + String.format("%e", maxThresh) + " | Time: " + String.format("%d", System.currentTimeMillis() - startTime));
		}
		return System.currentTimeMillis() - startTime;
	}

	public long pageRank(boolean[][] matrix, double threshold){
		if(!Arrays.deepEquals(matrix, cacheMattrix)){
			init(matrix);
			cacheMattrix = matrix;
		} else {//start from scratch
			for (String page: ranks.keySet()){
				ranks.put(page, null);
			}
		}

		int stepCounter = 0;
		HashMap<String, Double> oldRanks = null;
		System.out.println("Iterating step " + String.format("%d", stepCounter));
		long startTime = System.currentTimeMillis();
		while (step()){
			boolean underThreshold = true;
			double maxThresh = 0;
			if(oldRanks != null && oldRanks.size() > 0){
				for (Map.Entry<String, Double> oldRank: oldRanks.entrySet()){
					double currThresh = Math.abs(oldRank.getValue() - ranks.get(oldRank.getKey())) / oldRank.getValue();

					if (maxThresh < currThresh){
						maxThresh = currThresh;
					}

					if (maxThresh < threshold){
						underThreshold = true;
					} else {
						underThreshold = false;
						break;
					}
				}
			} else {
				underThreshold = false;
			}
			if (underThreshold){
				System.out.println("Iterating step " + String.format("%d", stepCounter) + " | UNDER THRESHOLD! : " + String.format("%e", maxThresh) + " | Time: " + String.format("%d", System.currentTimeMillis() - startTime));
				break;
			} else {
				oldRanks = (HashMap<String, Double>) ranks.clone();
			}
			System.out.println("Iterating step " + String.format("%d", ++stepCounter) + " | Current threshold: " + String.format("%e", maxThresh) + " | Time: " + String.format("%d", System.currentTimeMillis() - startTime));
		}
		return System.currentTimeMillis() - startTime;
	}

	public void init(String rootPath) throws IOException {//Opens up a new directory of html files and generates the adjacency matrix
		Path path = new File(rootPath).toPath();

		//Initialize everything
		adjMatrix = new HashMap<>();
		inLinks = new HashMap<>();
		ranks = new HashMap<>();

		if (Files.isDirectory(path)) {
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

					//Parse all outgoing links out of the html
					if (!file.toString().contains("~")){//Don't include talk, category, or user pages
						Document page = Jsoup.parse(file.toFile(), "UTF-8", "test.com/one/two/three");
						Elements outLinks = page.select("a[href]");

						//Add the page to the database if it does not already exist
						int subtitleIndex = page.title().indexOf('-') - 1;
						if (subtitleIndex >= 0){
							String title = page.title().substring(0, subtitleIndex);
							if(!ranks.containsKey(title)){
								ranks.put(title, null);//I can't put down the default value because I don't know it yet (1/N)
								adjMatrix.put(title, new ArrayList<>());
								inLinks.put(title, new ArrayList<>());
							}

							for ( org.jsoup.nodes.Element link: outLinks){
								if(link.text().length() != 0 && link.attr("abs:href").length() == 0 && !link.attr("href").contains("%7E") && link.attr("href").contains("../articles")){//Filter out links that don't point to the pages that I've downloaded
									if (!link.text().equals("Page") || (link.text().equals("Page") && (link.attr("href").contains("page") || link.attr("href").contains("Page")))){//There are lots of links pointing to "Page", but not the wikipedia page on "pages"
										//System.out.println(link.text() + " | " +  link.attr("href"));
										if(!ranks.containsKey(link.text())){
											ranks.put(link.text(), null);//I can't put down the default value because I don't know it yet (1-d/N)
											adjMatrix.put(link.text(), new ArrayList<>());
											inLinks.put(link.text(), new ArrayList<>());
										}

										inLinks.get(link.text()).add(title);
										adjMatrix.get(title).add(link.text());
									}
								}
							}
						}
					}
					return FileVisitResult.CONTINUE;
				}
			});
		}
	}

	public void init(boolean[][] matrix){
		//Initialize everything
		adjMatrix = new HashMap<>();
		inLinks = new HashMap<>();
		ranks = new HashMap<>();

		for (int i = 0; i < matrix.length; i++){
			String title = String.format("Page %d", i);
			ranks.put(title, null);//I can't put down the default value because I don't know it yet (1/N)
			if (!adjMatrix.containsKey(title)){
				adjMatrix.put(title, new ArrayList<>());
			}

			if (!inLinks.containsKey(title)){
				inLinks.put(title, new ArrayList<>());
			}
			System.out.println("Adding " + title);
			for (int j = 0; j < matrix[i].length; j++){
				if (matrix[i][j]){
					String outlink = String.format("Page %d", j);
					adjMatrix.get(title).add(outlink);
					if(!inLinks.containsKey(outlink)){
						inLinks.put(outlink, new ArrayList<>());
					}
					inLinks.get(outlink).add(title);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private boolean step(){
        /*Basic PageRank Update Rule: Each page divides its current PageRank equally
        across its out-going links, and passes these equal shares to the pages it points
        to. (If a page has no out-going links, it passes all its current PageRank to
        itself.) Each page updates its new PageRank to be the sum of the shares it
        receives.*/

		/*Scaled PageRank Update Rule: First apply the Basic PageRank Update Rule.
		Then URLChance down all PageRank values by a factor of s. This means that the total
		PageRank in the network has shrunk from 1 to s. We divide the residual 1 ? s
		units of PageRank equally over all nodes, giving (1 ? s)/n to each.*/

		//The above two paragraphs are taken from http://www.cs.cornell.edu/home/kleinber/networks-book/networks-book-ch14.pdf pg 407 and pg 409

		boolean changed = false;//if a step changes the page ranks
		Set<String> keys = ranks.keySet();
		int N = ranks.size();
		double defRank = 1 / N;//the rank for step 0
		double URLChance = (1 - d) / N;//Only used for non-naive implementations
		HashMap<String, Double> newRanks = new HashMap<>();
		boolean zeroeth = false;
		for(String page: keys){
			if (ranks.get(page) != null && !zeroeth) {
				/*if(adjMatrix.get(page).size() != 0) {//if there are some outgoing links
					double portion = ranks.get(page) / adjMatrix.get(page).size();
					for (String outgoing : adjMatrix.get(page)) {
						if (ranks.containsKey(outgoing)) {
							if (naive) {
								if (!newRanks.containsKey(page)) {
									newRanks.put(page, 0.0);
								}
								if (!newRanks.containsKey(outgoing)) {
									newRanks.put(outgoing, 0.0);
								}
								newRanks.put(outgoing, newRanks.get(outgoing) + portion);
							} else {
								if (!newRanks.containsKey(page)) {
									newRanks.put(page, URLChance);
								}

								if (!newRanks.containsKey(outgoing)) {
									newRanks.put(outgoing, URLChance);
								}
								newRanks.put(outgoing, newRanks.get(outgoing) + (portion * d));
							}
						}
					}
				}*/
				//Inlinking method
				newRanks.put(page, URLChance);//chance of typing url
				if (adjMatrix.get(page).size() == 0){//Nodes that don't have any links can't lose surfers
					newRanks.put(page, newRanks.get(page) + d * ranks.get(page));
				}
				for (String inLink : inLinks.get(page)){
					newRanks.put(page, newRanks.get(page) + d * ranks.get(inLink) / ((double) adjMatrix.get(inLink).size()));
				}
			} else {//This is step 0
				newRanks.put(page, defRank);
				zeroeth = true;
			}
		}
		for (String key: ranks.keySet()){
			if (ranks.get(key) != newRanks.get(key)){
				changed = true;
				ranks = newRanks;
				break;
			}
		}
		return changed;
	}

	public HashMap<String, Double> getRanks(){
		return ranks;
	}
}
