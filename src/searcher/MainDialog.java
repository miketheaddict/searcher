package searcher;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class MainDialog extends JDialog {
	private JPanel contentPane;
	private JButton prepButton;
	private JTextField searchBar;
	private JButton searchButton;
	private JLabel statusLabel;
	private JLabel indexLabel;
	private JList resultList;
	private JButton calcRankButton;
	private JButton rankButton;
	private JButton loadDirButton;
	private JSlider threshSlider;
	private JLabel thresholdLabel;
	private JCheckBox useDirCheckBox;
	private JSpinner matrixSizeSpinner;

	private JFileChooser chooser;

	private Searcher searcher = new Searcher();
	private PageRanker pageRanker = new PageRanker();
	String [] searchResults = null;
	String loadedIndex;

	long elapsed = System.currentTimeMillis();

	Timer animTimer = new Timer(250, new ActionListener() {
		String base = null;
		int number = 0;
		boolean direction;

		public void actionPerformed(ActionEvent e) {
			if (base == null) {
				base = statusLabel.getText();
			}
			if (number <= 0) {
				direction = true;
			} else if (number >= 3) {
				direction = false;
			}
			if (direction) {
				number++;
			} else {
				number--;
			}

			String output = base;
			for (int i = 0; i < number; i++) {
				output += ".";
			}
			statusLabel.setText(output);
		}
	});

	public MainDialog() {
		setContentPane(contentPane);
		setModal(true);
		getRootPane().setDefaultButton(searchButton);

		prepButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (loadedIndex != null){
					onPrep();
				} else {
					statusLabel.setText("Please select a directory first!");
				}
			}
		});

		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loadedIndex != null){
					search(searchBar.getText());
				} else {
					statusLabel.setText("Please select a directory first!");
				}
			}
		});

		calcRankButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loadedIndex != null || !useDirCheckBox.isSelected()) {
					onCalc();
				} else {
					statusLabel.setText("Please select a directory first!");
				}
			}
		});

		rankButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loadedIndex != null || !useDirCheckBox.isSelected()){
					rank();
				} else {
					statusLabel.setText("Please select a directory first!");
				}
			}
		});

		loadDirButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadDir();
			}
		});
		threshSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				thresholdLabel.setText(String.format("Threshold: 1e-%3d", threshSlider.getValue()));
			}
		});
	}

	private void loadDir() {
		if (chooser == null){
			chooser = new JFileChooser(System.getProperty("user.dir"));
			chooser.setDialogTitle("Pick an existing index or a new directory");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.addChoosableFileFilter(new FileNameExtensionFilter("Index Files or Directories", "txt"));
			chooser.setAcceptAllFileFilterUsed(false);
		}

		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			loadedIndex = chooser.getSelectedFile().toString();
			if (loadedIndex != null){
				indexLabel.setText(loadedIndex);
			} else {
				indexLabel.setText("No index or directory loaded...");
			}
		}

		searcher = new Searcher();
		pageRanker = new PageRanker();
	}

	private void onPrep() {
		statusLabel.setText("Loading " + loadedIndex);
		animTimer.start();
		elapsed = System.currentTimeMillis();

		new Thread(new Runnable() {
			@Override
			public void run() {
				//Run load here
				searcher.indexDirectory(new File(loadedIndex));

				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						elapsed = System.currentTimeMillis() - elapsed;
						long second = (elapsed / 1000) % 60;
						long minute = (elapsed / (1000 * 60)) % 60;
						long hour = (elapsed / (1000 * 60 * 60)) % 24;
						String elapsedFormatted = String.format("%d hours %d minutes %d seconds %d milliseconds", hour, minute, second, elapsed);
						animTimer.stop();
						statusLabel.setText("Load Complete in " + elapsedFormatted + "!");
						//Done Loading
					}
				});
			}
		}).start();
	}

	private void onCalc(){
		if(loadedIndex != null){
			statusLabel.setText("Calculating Rank on " + loadedIndex);
		} else if (!useDirCheckBox.isSelected()){
			statusLabel.setText("Calculating Rank on randomly generated graph");
		}
		animTimer.start();
		elapsed = System.currentTimeMillis();

		new Thread(new Runnable() {
			@Override
			public void run() {
				//Run PageRank here
				int repeats = 5;
				if(useDirCheckBox.isSelected() && loadedIndex != null && new File(loadedIndex).isDirectory()){
					long avg = 0;
					for (int i = 0; i < repeats; i++){
						avg += pageRanker.pageRank(loadedIndex, 1/Math.pow(10.0, threshSlider.getValue()));
					}
					System.out.println(String.format("Average Time out of %d: %s", repeats, ((double) avg) / ((double)repeats)));
				} else if(!useDirCheckBox.isSelected()) {
					int matrixSize = (int) matrixSizeSpinner.getValue();
					boolean[][] matrix = new boolean[matrixSize][matrixSize];
					Random randomizer = new Random();
					for (int i = 0; i < matrixSize; i++){
						for (int j = 0; j < matrixSize; j ++){
							matrix[i][j] = randomizer.nextBoolean();
						}
					}

					long avg = 0;
					for (int i = 0; i < repeats; i++){
						avg += pageRanker.pageRank(matrix, 1/Math.pow(10.0, threshSlider.getValue()));
					}
					System.out.println(String.format("Average Time out of %d: %s", repeats, ((double) avg) / ((double)repeats)));
				}

				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						elapsed = System.currentTimeMillis() - elapsed;
						long second = (elapsed / 1000) % 60;
						long minute = (elapsed / (1000 * 60)) % 60;
						long hour = (elapsed / (1000 * 60 * 60)) % 24;
						long millis = (elapsed) - second * 1000 - minute * 60 * 1000 - hour * 60 * 60 * 1000;
						String elapsedFormatted = String.format("%d hours %d minutes %d seconds %d milliseconds", hour, minute, second, millis);
						animTimer.stop();

						statusLabel.setText("Ranking completed in " + elapsedFormatted);
						//Done Loading
					}
				});
			}
		}).start();
	}

	private void search(String searchTerms){
		statusLabel.setText("Searching");
		animTimer.start();
		elapsed = System.currentTimeMillis();

		new Thread(new Runnable() {
			@Override
			public void run() {
				//Run search here
				searchResults = searcher.search(searchBar.getText());
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						elapsed = System.currentTimeMillis() - elapsed;
						long second = (elapsed / 1000) % 60;
						long minute = (elapsed / (1000 * 60)) % 60;
						long hour = (elapsed / (1000 * 60 * 60)) % 24;
						String elapsedFormatted = String.format("%d hours %d minutes %d seconds %d milliseconds", hour, minute, second, elapsed);
						animTimer.stop();
						resultList.setListData(searchResults);
						statusLabel.setText("Search Complete in " + elapsedFormatted + "!");
						//Done Searching
					}
				});
			}
		}).start();
	}

	private void rank(){
		ListModel comps = resultList.getModel();
		if (comps != null && comps.getSize() > 0 && comps.getElementAt(0).toString().contains("/")){
			HashMap<String, Double> ranks = pageRanker.getRanks();
			if (ranks != null && ranks.size() > 0){

				Map<String, Double> unsortedRanks = new HashMap<>();

				for (int i = 0; i < comps.getSize(); i++){
					String compText = comps.getElementAt(i).toString();
					if (compText.indexOf('|') >= 0){
						compText = compText.substring(compText.indexOf('|') + 2);
					}
					Document page = null;
					try {
						page = Jsoup.parse(new File(compText), "UTF-8", "test.com/one/two/three");
					} catch (IOException e) {
						e.printStackTrace();
					}

					if(page != null){
						int subtitleIndex = page.title().indexOf('-') - 1;
						if (subtitleIndex >= 0) {
							String title = page.title().substring(0, subtitleIndex);

							unsortedRanks.put("(" + String.format("%e", ranks.get(title)) + ") " + title + " | " + compText, ranks.get(title));

						}
					}
				}

				HashMap<String, Double> sortedRanks = (HashMap<String, Double>) MapUtil.sortByValue(unsortedRanks);
				resultList.setListData(sortedRanks.keySet().toArray(new String[sortedRanks.size()]));
			} else {
				statusLabel.setText("Please calculate PageRanks first");
			}
		} else {
			Map<String, Double> unsortedRanks = pageRanker.getRanks();
			String[] formattedArray = new String[unsortedRanks.size()];
			int counter = 0;
			double sum = 0;
			int edges = 0;
			int noOut = 0;
			int noIn = 0;
			int loner = 0;
			for (Map.Entry rank : MapUtil.sortByValue(unsortedRanks).entrySet()){
				sum += (Double) rank.getValue();
				formattedArray[counter++] = "(" + String.format("%e", (Double) rank.getValue()) + ") " + rank.getKey();
				edges += pageRanker.adjMatrix.get(rank.getKey()).size();
				boolean both = false;
				if(pageRanker.adjMatrix.get(rank.getKey()).size() == 0){
					noOut++;
					both = true;
				}
				if(pageRanker.inLinks.get(rank.getKey()).size() == 0){
					noIn++;
					if(both){
						loner++;
					}
				}
			}
			resultList.setListData(formattedArray);

			statusLabel.setText(String.format("Nodes: %d Edges: %d Sum: %e Pages with no outgoing: %d Pages with no incoming: %d Isolated Pages: %d", unsortedRanks.size(), edges, sum, noOut, noIn, loner ));
		}
	}

	public static void main(String[] args) {
		MainDialog dialog = new MainDialog();
		dialog.pack();
		dialog.setVisible(true);
		System.exit(0);
	}
}
