package searcher;

import jdk.internal.org.objectweb.asm.tree.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Michael on 5/6/2015.
 * This class uses Lucene to index a set of documents and builds an adjacency matrix out of the documents it indexes
 */
public class Searcher {
	String indexPath = "searchIndex";
	String matrixPath = "adjMatrix.mtx";

	IndexSearcher indexSearcher;
	QueryParser parser;

	public Searcher() {

	}

	String indexDirectory(File path) {//A lot is borrowed rom Apache's website
		boolean success = true;
		Path docDir = path.toPath();
		Directory dir = null;
		try {
			dir = FSDirectory.open(Paths.get(indexPath));
		} catch (IOException e) {
			success = false;
			e.printStackTrace();
		}

		if (success) {
			StandardAnalyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

			IndexWriter writer = null;
			try {
				writer = new IndexWriter(dir, iwc);
			} catch (IOException e) {
				success = false;
				e.printStackTrace();
			}

			if (success) {
				try {
					indexDocs(writer, docDir);
				} catch (IOException e) {
					success = false;
					e.printStackTrace();
				}
			}
			if (writer != null){
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (success) {
			loadIndex(new File(indexPath));
			return indexPath;
		} else {
			return null;
		}
	}

	String loadIndex(File index) {
		IndexReader reader = null;
		try {
			reader = DirectoryReader.open(FSDirectory.open(Paths.get(index.toString())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		indexSearcher = new IndexSearcher(reader);
		StandardAnalyzer analyzer = new StandardAnalyzer();
		parser = new QueryParser("contents", analyzer);
		return index.toString();
	}

	String[] search(String searchTerms) {
		if (indexSearcher != null && parser != null){
			ArrayList<Integer> results = new ArrayList<>();
			TopScoreDocCollector collector = TopScoreDocCollector.create(10000);
			/*SimpleCollector collector = new SimpleCollector(){
				@Override
				public boolean needsScores() {
					return false;
				}

				@Override
				public void collect(int i) throws IOException {
					results.add(i);
				}
			};*/
			ScoreDoc[] hits = null;
			String[] output;
			try {
				 indexSearcher.search(parser.parse(searchTerms), collector);
				hits = collector.topDocs().scoreDocs;
				output = new String[hits.length];
			} catch (ParseException | IOException e) {
				e.printStackTrace();
				output = new String[indexSearcher.getIndexReader().numDocs()];
				for (int i = 0; i < indexSearcher.getIndexReader().numDocs(); i++){
					try {
						output[i] = indexSearcher.doc(i).get("path");
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
			/*String[] output = new String[results.size()];
			for (int i = 0; i < results.size(); i ++){
				try {
					output[i] = indexSearcher.doc(results.get(i)).get("path");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}*/
			if (hits != null){
				for (int i = 0; i < hits.length; i++){
					try {
						output[i] = indexSearcher.doc(hits[i].doc).get("path");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			return output;


		} else {
			return new String[]{"Error! No Index or Directory Loaded!"};
		}
	}

	static void indexDocs(final IndexWriter writer, Path path) throws IOException {

		if (Files.isDirectory(path)) {
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

					//Parse all outgoing links out of the html
					if (!file.toString().contains("~")) {//Don't include talk, category, or user pages
						org.jsoup.nodes.Document page = Jsoup.parse(file.toFile(), "UTF-8", "test.com/one/two/three");

						//Add the page to the database if it does not already exist
						int subtitleIndex = page.title().indexOf('-') - 1;
						if (subtitleIndex >= 0) {
							String title = page.title().substring(0, subtitleIndex);

							System.out.println("Indexing page on " + title + " \t| " + file.toString());
							try {
								indexDoc(writer, file, attrs.lastModifiedTime().toMillis());
							} catch (IOException ignore) {
								// don't index files that can't be read.
							}
						}
					}
					return FileVisitResult.CONTINUE;
				}
			});
		}
	}


	//Everything below is taken directly from Apache
	/**
	 * Indexes a single document
	 */
	static void indexDoc(IndexWriter writer, Path file, long lastModified) throws IOException {
		try (InputStream stream = Files.newInputStream(file)) {
			// make a new, empty document
			Document doc = new Document();

			// Add the path of the file as a field named "path".  Use a
			// field that is indexed (i.e. searchable), but don't tokenize
			// the field into separate words and don't index term frequency
			// or positional information:
			Field pathField = new StringField("path", file.toString(), Field.Store.YES);
			doc.add(pathField);

			// Add the last modified date of the file a field named "modified".
			// Use a LongField that is indexed (i.e. efficiently filterable with
			// NumericRangeFilter).  This indexes to milli-second resolution, which
			// is often too fine.  You could instead create a number based on
			// year/month/day/hour/minutes/seconds, down the resolution you require.
			// For example the long value 2011021714 would mean
			// February 17, 2011, 2-3 PM.
			doc.add(new LongField("modified", lastModified, Field.Store.NO));

			// Add the contents of the file to a field named "contents".  Specify a Reader,
			// so that the text of the file is tokenized and indexed, but not stored.
			// Note that FileReader expects the file to be in UTF-8 encoding.
			// If that's not the case searching for special characters will fail.
			doc.add(new TextField("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));

			if (writer.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
				// New index, so we just add the document (no old document can be there):
				//System.out.println("adding " + file);
				writer.addDocument(doc);
			} else {
				// Existing index (an old copy of this document may have been indexed) so
				// we use updateDocument instead to replace the old one matching the exact
				// path, if present:
				//System.out.println("updating " + file);
				writer.updateDocument(new Term("path", file.toString()), doc);
			}
		}
	}
}
